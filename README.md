# EESTech Challenge Wordpress

This is the backup of the EC website created by Duplicator (Istructions also [here](https://snapcreek.com/duplicator/docs/quick-start)).

## To restore the backup

1. Copy the zip file and the `installer.php` behind a reverse proxy (in our case nginx using the ansible playbook).
2. In web browser open installer.php file on destination site (for example https://ec.eestec.net/installer.php).
3. Go through the steps explained on the website.
4. ???
5. Profit.